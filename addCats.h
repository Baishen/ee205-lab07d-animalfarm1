///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Baishen Wang<baishen@hawaii.edu>
/// @date   @todo 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern int addCat( char nameToAdd[],
                   enum Gender isGender,
                   enum Breed isBreed,
                   bool isFixedNew,
                   float weightNew,
                   enum Color collarColor1,
                   enum Color collarColor2,
                   unsigned long long license ) ;

