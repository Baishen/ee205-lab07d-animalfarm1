///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Baishen Wang<baishen@hawaii.edu>
/// @date   @todo 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

extern char* colorCollar( const int color );

extern char* genderOfCat( const int gender );

extern void printCat( const int index );

extern void printAllCats();

extern int findCats( char nameToFind[] );


